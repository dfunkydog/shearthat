SHEARTHAT.COM

Takes a long tweet slices it in appropriate chunk sizes, ads anotations 
and updates to twitter.

It will send replies if a twitter handle is the *1st* word in the tweet.
Alternatively you can use the twitter status id (the numerical string at 
the end of a twitter link) to make sure the replies are in the 
conversation´s thread.
