<?php session_start();
require 'twitteroauth/autoloader.php';
require "config.php";
use Abraham\TwitterOAuth\TwitterOAuth;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js ie6 oldie" lang="en">
  <![endif]-->
  <!--[if IE 7]>
  <html class="no-js ie7 oldie" lang="en">
    <![endif]-->
    <!--[if IE 8]>
    <html class="no-js ie8 oldie" lang="en">
      <![endif]-->
      <!--[if gt IE 8]>
      <!-->
      <html lang="en">
        <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>Shear your long tweet into multiple shorter chunks</title>
    <meta name="viewport" content="width=device-width;  initial-scale=1.0; ">
    <meta name="description" content="Shearthat will split your long tweet into multiple smaller tweets of 140 characters or less, automatically annotate and publishes them to twitter." />
    <meta name="author" content="Michael Dyer">

    <link rel="icon" type="image/png" href="http://www.416studios.co.uk/favicon.ico" />
    <link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>

    <style>
    @font-face{font-family:'Yanone Kaffeesatz';font-style:normal;font-weight:400;src:local("Yanone Kaffeesatz Regular"),local("YanoneKaffeesatz-Regular"),url(http://fonts.gstatic.com/s/yanonekaffeesatz/v7/YDAoLskQQ5MOAgvHUQCcLfGwxTS8d1Q9KiDNCMKLFUM.woff2) format("woff2"),url(http://fonts.gstatic.com/s/yanonekaffeesatz/v7/YDAoLskQQ5MOAgvHUQCcLRTHiN2BPBirwIkMLKUspj4.woff) format("woff")}.visuallyhidden{clip:rect(0 0 0 0)}.visuallyhidden.focusable:active,.visuallyhidden.focusable:focus{clip:auto;height:auto;margin:0;overflow:visible;position:static;width:auto}html{min-height:100%;background:#689F38 -webkit-radial-gradient(center,ellipse,#8bc34a 0%,#689f38 100%)no-repeat;background:#689F38 radial-gradient(ellipse at center,#8bc34a 0%,#689f38 100%)no-repeat;background-size:cover;box-sizing:border-box}*,*:before,*:after{box-sizing:inherit}body{color:#dfdfdf;line-height:1.6;font-family:'Yanone Kaffeesatz',arial,helvetica,sans-serif;font-size:20px}#wrap{width:calc(100% - 10px);max-width:800px;margin:0 auto}.flexbox #wrap{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;min-height:100vh;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column}#main{-webkit-box-flex:1;-webkit-flex:1;-ms-flex:1;flex:1}a{color:#dfdfdf;text-decoration:none}p{word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;-ms-hyphens:auto;hyphens:auto}input,textarea{font-family:'Yanone Kaffeesatz',sans-serif;font-size:1em;display:block;border:1px solid #689F38;padding:.5em;background-color:#DCEDC8;color:#444}input[type='submit'],input[type='reset'],button,.button{-webkit-transition:all 350ms ease-out;transition:all 350ms ease-out;color:#444;background:#FFC107;padding:.5em;cursor:pointer}input:disabled{opacity:.2;cursor:default}textarea{width:100%}ul,li{padding:0}:-ms-input-placeholder{color:#727272;opacity:.4}::-moz-placeholder{color:#727272;opacity:.4}::-webkit-input-placeholder{color:#727272;opacity:.4}a.tip{margin-top:200px}p>a.button{text-align:center;min-width:160px;width:35%;display:block;margin:0 auto}form{position:relative;margin-top:48px}input[type='submit']{position:absolute;top:-48px;right:0}input[type='reset'],.advanced{display:none}.advanced{width:100px;color:#dfdfdf;background-color:transparent;padding:0}.advanced::after{position:relative;top:19px;left:4px;content:'';width:0;height:0;border-style:solid;border-width:9px 6px 0 6px;border-color:#dfdfdf transparent transparent;-webkit-transform:rotate(360deg);-ms-transform:rotate(360deg);transform:rotate(360deg)}.visuallyhidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.visuallyhidden,.reply-to{-webkit-transition:all 350ms ease-out;transition:all 350ms ease-out}.reply-to input{width:100%;max-width:320px}#info-area p{border:1px solid #dfdfdf;font-size:.8em;padding:.5em}#info-area p.warning{background:#8C2929;padding:.5em;border:1px solid #8C2929;color:#b6b6b6}#footer{margin-top:2em}#footer li{display:inline-block;font-size:.75em;list-style-type:none;margin-right:2em}@media screen and (min-aspect-ratio:16/6){form{margin-top:0}textarea{width:90%}input[type='submit']{width:10%;top:0}}@media screen and (min-width:816px) and (max-aspect-ratio:16/6){form{margin-top:0}input[type='submit']{width:auto;position:relative;top:auto;margin-top:1em}}@media screen and (min-width:600px) and (min-height:680px){.advanced{display:block}form{margin-top:0}input[type='submit']{width:10%;top:0}}
    </style>

	<script>  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-1264176-7']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>

</head>
<body>

<div  id="wrap">
  <div id="header">

    <h1>Shearthat</h1>
    <?php if( !isset( $_SESSION['status']) ){ ?>
    <p>
      Shears your long tweet into multiple smaller tweets of 140 characters or less, automatically annotate and publishes them to twitter.
    </p>
  <?php } ?>
  </div>
  <!--End Header-->
  <div id="main" role="main">

	<?php

		if(isset( $_SESSION['status']) && ( $_SESSION['status'] == 'verified') ){
			include 'form.php';
          } elseif( isset( $_SESSION['status']) && ( $_SESSION['status'] == 'denied') ){
            echo '<p class="error">Sorry, we couldn\'t get authorisation from twitter</p>';
            echo '<p><a href="redirect.php" class="button" role="button">Try Again?</a></p>';
            /* clear sessions */
            unset( $_SESSION['status'] ) ;
          } else {
            echo '<p><a href="redirect.php" class="button" role="button">Authorise with Twitter</a></p>';
          }

	?>

  </div>
    <!--end main content-->
  <div id="footer">


    <ul>
      <li>

        <a href="http://www.416studios.co.uk">&copy;  <?php echo date("Y"); ?>  416studios</a>
      </li>
      <li>
        <a href="http://twitter.com/416studios">shearthat on twitter</a>
      </li>
      <li>
        <a href="clearsessions.php">Log out</a>
      </li>
    </ul>

  </div>
<script src="js/min.js"></script>
</body>