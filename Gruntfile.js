module.exports = function(grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            options: {
              separator: ';',
            },
            dist: {
              src: ['js/modernizr.js','js/site.js'],
              dest: 'js/main.js',
            }
        },
        uglify: {
            build: {
                src: 'js/main.js',
                dest: 'js/min.js'
            }
        },
        csso:{
            compress:{
                files:{
                    'main.css': ['site.css']
                }
            },
            restructure:{
                restructure:false
            }
        }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-csso');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat','uglify','csso']);

};