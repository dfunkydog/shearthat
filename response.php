<?php
session_start();


/**
* @file
* User has successfully authenticated with Twitter. Access tokens saved to session and DB.
*/

/* Load required lib files. */

require 'twitteroauth/autoloader.php';
require "config.php";
use Abraham\TwitterOAuth\TwitterOAuth;

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: clearsessions.php');
    echo "not signed in";
}

/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);


function chunker($slab, $reply_handle)
{
    $reply_to = '';
    $cslab = preg_replace("/[\n\r]/", "", $slab);
    preg_match_all("/(\G@\w+ )/", $slab, $replying);
    $replyin = $replying[0];
    $reply_to = implode(" ", $replyin);
    $cleanslab = str_replace($replyin, " ", $cslab);
    if(isset($reply_handle) ){$reply_to .= $reply_handle;}

    if(strlen($reply_to)+strlen($cleanslab) > 140){
        if ($reply_to) {$mod = 134-(strlen($reply_to));} else {$mod = 134;}
        //chunk the cleanslab and lay the chunks out into an array
        $readyslab = wordwrap($cleanslab, $mod, "\n", false);
        $chunks = explode("\n", $readyslab);

        $total_chunks = count($chunks);
        $counter = 1;

        foreach ($chunks as &$chunk) {
            if ($counter == 1) {
            $chunk = $reply_to.$chunk.'…'.$counter.'/'.$total_chunks;
            } elseif ($counter == $total_chunks) {
                $chunk = $reply_to.'…'.$chunk.' '.$counter.'/'.$total_chunks;
            } else {
                $chunk = $reply_to.'…'.$chunk.'…'.$counter.'/'.$total_chunks;
            }
            $counter++;
        }

        return $chunks;
    } else {
        $the_slab  = array('reply' => $reply_to.' '.$cleanslab);
        return $the_slab;
    }
}

function get_reply_to_details($status){

        $details = explode('/',$status);
        $t= array_search('twitter.com', $details );
        $t = $details[$t+1];
        $handle = '@' . $t;
        $status_id = end($details);
        return array($handle,$status_id);

}

if (isset($_POST['update_status'])) {
    $the_tweet = $_POST['the_tweet'];
    $error_count = 0;
    $response_code = array();
    if( strlen($_POST['reply_to_id']) > 30 ){
        list($handle, $status_id) = get_reply_to_details($_POST['reply_to_id']);
    }
    $replying_to_id = $status_id;
    $tweets = chunker($the_tweet, $handle);
    if (isset($reverse)){krsort($tweets);}
    $parts = count($tweets);
    $part = -1;

    foreach ($tweets as $chunk) {
      $part++;
      usleep(200000);
      $attempts = 1;
      $connection->post('statuses/update', array('status' => stripslashes($chunk), 'in_reply_to_status_id' => $replying_to_id));
      while ($connection->getLastHttpCode() != 200 && $attempts <= 4){
        $connection->post('statuses/update', array('status' => stripslashes($chunk), 'in_reply_to_status_id' => $replying_to_id));
        $attempts++;
        if($attempts == 5){
            $error_code = $connection->getLastBody()->errors[0]->code;
            $error_message = $connection->getLastBody()->errors[0]->message;
          break 2;}
      }
    }

    $_SESSION['published_count'] = $part ;
    if(isset($error_code) ){
        $_SESSION['response_code'] = $error_code;
    } else {
        $_SESSION['response_code'] = 'tweeted';
    }
    if(isset($error_message) ) {$_SESSION['error_message'] = $error_message;}
    $_SESSION['tweets'] = $tweets;


    header("Location: /index.php") ;
}


?>