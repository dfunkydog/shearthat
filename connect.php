<!DOCTYPE html>

<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <title>Shear your long tweets into multiple shorter chunks</title>
  <meta name="viewport" content="width=device-width;  initial-scale=1.0; ">
  <meta name="description" content="Shearthat will split your long tweet into multiple smaller tweets of 140 characters or less, automatically anotate and publishes them to twitter." />
  <meta name="author" content="Michael Dyer">

  <link rel="icon" type="image/png" href="http://www.416studios.co.uk/favicon.ico" />

   <link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" href="stylesheets/colorbox.css" type="text/css" media="screen" />

  <script src="http://ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.0.6-development-only.js"></script>
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-1264176-7']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
</head>
<body>
    <div  id="wrap">
        <div id="header">


               <h1>Shearthat</h1>


        </div>
        <!--End Header-->
        <div id="main" role="main">

           <p>Shears your long tweet into multiple smaller tweets of 140 characters or less, automatically anotate and publishes them to twitter.</p>
<?php

/**
 * @file
 * Check if consumer token is set and if so send user to get a request token.
 */

/**
 * Exit with an error message if the CONSUMER_KEY or CONSUMER_SECRET is not defined.
 */
session_start();
require_once('config.php');
if (CONSUMER_KEY === '' || CONSUMER_SECRET === '') {
  echo 'There is a problem with shearthat. <b>Consumer key not defined or corrupted</b> <a href="twitter.com/shearthat">contact shearthat on twitter</a>';
  exit;
}
?>

<section>
  <div class="button">
    <a class="btn btn-large btn-primary" href="./redirect.php">Sign in with twitter</a>
  </div>
  <div class="cover">
    <div class="innie"><img src="images/open.svg" alt=""></div>
    <div class="spine"></div>
    <div class="outie"><img src="images/open.svg" alt=""></div>
</div>
  <div class="shadow"></div>
</section>







        </div>        <!--end main content-->
  <div id="footer">
    <ul>
      <li>
        &copy; <?php echo date('Y'); ?> <a href="http://www.416studios.co.uk"> 416studios</a>
      </li>
      <li>
        <a href="http://twitter.com/416studios">shearthat on twitter</a>
      </li>

      <li>
        <a href="clearsessions.php">Log out</a>
      </li>
    </ul>
  </div>

  </div>
  <!--end wrap-->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <!-- Add fancyBox -->

  <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>

  <script src="js/scripts.js"></script>
</body>
</html>