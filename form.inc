<div class="info-area">

<?php


if(!$_SESSION['status_index']){
  echo '<h3>Start</h3>
            <p>By typing your long tweet below.</p>';
          }
else {
  print_r($_SESSION);
  if(isset($_SESSION['response_error'])){ ?>
    <p class="warning">Due to a <a href="https://dev.twitter.com/docs/error-codes-responses"><?php echo $_SESSION['response_error']; ?></a> error, update  <?php echo $_SESSION['status_index'] ;?> has failed.<br />In order to preserve the chronology of your updates we've stopped processing the subsequent updates<br/>
    You can try copy &amp; pasting the tweets manually</p>

    <?php

    $unpublished = $_SESSION['unpublished'];
    foreach ($unpublished as $failed) {
      echo '<p>' . $failed . '</p>';
    }
  } else {
    sleep(2.5);
    $status_updates = $connection->get('statuses/user_timeline', array('count' => $_SESSION['published']));
      foreach($status_updates as $check){
      if ($check->source =='<a href="http://www.shearthat.com" rel="nofollow">ShearThat</a>'){
      echo '<p>'.$check->text.'</p>';
      }

    }
  }
}
// clear unused variables

unset($_SESSION['response']);



?>
    </div>

    <form class="update-form" method="post" action="response.php">
      <div class="options">
        <label class="reverse">
          <input type="checkbox" <?php if($_POST['reverse']){echo 'checked="checked" '; } ?>name="reverse">Reverse</label>
        <p class="btn btn-small advanced">
          advanced &nbsp;<span class="picto">&#xf0d7;</span>
        </p>
      </div>
      <p class="reply-to">
        <label for="reply-to">
          <span>reply to</span>
          <input class="reply-to" name="reply_to"></label>
      </p>
      <p class="reply-to">
        <label for="reply-to-id">
          <span>reply to id</span>
          <input class="reply-to-id" name="reply_to_id"></label>
      </p>
      <textarea name="the_tweet" id="the_tweet" cols="32" rows="10"></textarea>
      <input type="submit" class="btn btn-large btn-primary" name="update_status" value="tweet">
      <input type="reset" class="btn btn-small refresh picto" value="&#xf021;"/>
    </form>
    <p>
      Selecting <strong><span class="label label-info">reverse</span></strong>
      will update your timeline with the last tweet first,
      <a class='gallery' href='reverse.png'>example</a>
    </p>