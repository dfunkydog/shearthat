(function(win, $) {
    $.fn.toggleClass = function( className, b ) {
        this.forEach( function( item ) {
            var classList = item.classList;
            if( typeof b !== 'boolean' ) {
                b = !classList.contains( className );
            }
            classList[ b ? 'add' : 'remove' ].apply( classList, className.split( /\s/ ) );
        });
        return this;
    };

    var delay = (function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function toggleSubmit(){
        var submitBtn = document.querySelectorAll('input[type="submit"]');
        if( $("textarea")[0].value != ''){
            submitBtn[0].disabled = false;
        } else {submitBtn[0].disabled = true;}
    }

$('textarea').on('keyup',function() {
        var preview = '<h3>Preview</h3>';
        var infoArea = document.getElementById('info-area');

        delay(function() {
            while ( infoArea.firstChild ) {
              infoArea.removeChild(infoArea.firstChild);
            }
            $('#info-area')[0].innerHTML += preview;

            var cleanslab = document.getElementById('the_tweet').value;
            cleanslab = cleanslab.replace(/[\r\n]+/gm, ' ');

            function wordwrap(str, width, brk, cut) {

                brk = brk || '\n';
                width = width || 75;
                cut = cut || false;

                if (!str) {
                    return str;
                }

                var regex = '.{1,' + width + '}(\\s|$)' + (cut ? '|.{' + width + '}|.+$' :
                    '|\\S+?(\\s|$)');

                return str.match(RegExp(regex, 'g')).join(brk);

            }
            var handle;
            if ((cleanslab.substr(0, 1) == '@') || (cleanslab.substr(0, 3).toUpperCase() ==
                    '#FF')) {
                handle = (cleanslab.substr(0,cleanslab.indexOf(' ')))+' ';


                cleanslab = cleanslab.substr(cleanslab.indexOf(' '));
            } else {
                handle = '';
            }

            if (typeof handle != 'undefined' && handle.length + cleanslab.length >
                140) {
                var apele = $('#info-area')[0];
                var readyslab = wordwrap(cleanslab, 135 - handle.length, '\n');
                var chunks = readyslab.split('\n');
                var chunklen = chunks.length;
                for (var k = 0; k < chunks.length; k++) {
                    if (k === 0) {
                        $('#info-area')[0].innerHTML +='<p>' + handle + chunks[k] + ' …' + (k + 1) + '/' +
                            chunklen + '</p>';
                    } else if (k == (chunklen - 1)) {
                        apele.innerHTML +='<p>' + handle + '…' + chunks[k] + ' ' + (k + 1) +
                            '/' + chunklen + '</p>';
                    } else {
                        apele.innerHTML +='<p>' + handle + '…' + chunks[k] + ' …' + (k + 1) +
                            '/' + chunklen + '</p>';
                    }
                };
            } else {
                if(cleanslab.length > 0 ){
                $('#info-area')[0].innerHTML +='<p>' + handle + ' ' + cleanslab + '</p>';
                }
            }
            toggleSubmit();


        }, 500);
    });

    $('form').on('contextmenu', function(e){
        toggleSubmit();
    });

    $(".advanced").on('click', function(e){
        $('p.reply-to').toggleClass('visuallyhidden');
    });

})(window, function(n,e,k,h,p,m,l,b,d,g,f,c){c=function(a,b){return new c.i(a,b)};c.i=function(a,d){k.push.apply(this,a?a.nodeType||a==n?[a]:""+a===a?/</.test(a)?((b=e.createElement(d||"q")).innerHTML=a,b.children):(d&&c(d)[0]||e).querySelectorAll(a):/f/.test(typeof a)?/c/.test(e.readyState)?a():c(e).on("DOMContentLoaded",a):a:k)};c.i[f="prototype"]=(c.extend=function(a){g=arguments;for(b=1;b<g.length;b++)if(f=g[b])for(d in f)a[d]=f[d];return a})(c.fn=c[f]=k,{on:function(a,d){a=a.split(h);this.map(function(c){(h[b=a[0]+(c.b$=c.b$||++p)]=h[b]||[]).push([d,a[1]]);c["add"+m](a[0],d)});return this},off:function(a,c){a=a.split(h);f="remove"+m;this.map(function(e){if(b=(g=h[a[0]+e.b$])&&g.length)for(;d=g[--b];)c&&c!=d[0]||a[1]&&a[1]!=d[1]||(e[f](a[0],d[0]),g.splice(b,1));else!a[1]&&e[f](a[0],c)});return this},is:function(a){d=(b=this[0])&&(b.matches||b["webkit"+l]||b["moz"+l]||b["ms"+l]);return!!d&&d.call(b,a)}});return c}(window,document,[],/\.(.+)/,0,"EventListener","MatchesSelector"));

