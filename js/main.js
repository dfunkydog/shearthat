/* Modernizr 2.8.3 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-flexbox-cssclasses-testprop-testallprops-domprefixes
 */
;window.Modernizr=function(a,b,c){function x(a){j.cssText=a}function y(a,b){return x(prefixes.join(a+";")+(b||""))}function z(a,b){return typeof a===b}function A(a,b){return!!~(""+a).indexOf(b)}function B(a,b){for(var d in a){var e=a[d];if(!A(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function C(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:z(f,"function")?f.bind(d||b):f}return!1}function D(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+n.join(d+" ")+d).split(" ");return z(b,"string")||z(b,"undefined")?B(e,b):(e=(a+" "+o.join(d+" ")+d).split(" "),C(e,b,c))}var d="2.8.3",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k,l={}.toString,m="Webkit Moz O ms",n=m.split(" "),o=m.toLowerCase().split(" "),p={},q={},r={},s=[],t=s.slice,u,v={}.hasOwnProperty,w;!z(v,"undefined")&&!z(v.call,"undefined")?w=function(a,b){return v.call(a,b)}:w=function(a,b){return b in a&&z(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=t.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(t.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(t.call(arguments)))};return e}),p.flexbox=function(){return D("flexWrap")};for(var E in p)w(p,E)&&(u=E.toLowerCase(),e[u]=p[E](),s.push((e[u]?"":"no-")+u));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)w(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},x(""),i=k=null,e._version=d,e._domPrefixes=o,e._cssomPrefixes=n,e.testProp=function(a){return B([a])},e.testAllProps=D,g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+s.join(" "):""),e}(this,this.document);;(function(win, $) {
    $.fn.toggleClass = function( className, b ) {
        this.forEach( function( item ) {
            var classList = item.classList;
            if( typeof b !== 'boolean' ) {
                b = !classList.contains( className );
            }
            classList[ b ? 'add' : 'remove' ].apply( classList, className.split( /\s/ ) );
        });
        return this;
    };

    var delay = (function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function toggleSubmit(){
        var submitBtn = document.querySelectorAll('input[type="submit"]');
        if( $("textarea")[0].value != ''){
            submitBtn[0].disabled = false;
        } else {submitBtn[0].disabled = true;}
    }

$('textarea').on('keyup',function() {
        var preview = '<h3>Preview</h3>';
        var infoArea = document.getElementById('info-area');

        delay(function() {
            while ( infoArea.firstChild ) {
              infoArea.removeChild(infoArea.firstChild);
            }
            $('#info-area')[0].innerHTML += preview;

            var cleanslab = document.getElementById('the_tweet').value;
            cleanslab = cleanslab.replace(/[\r\n]+/gm, ' ');

            function wordwrap(str, width, brk, cut) {

                brk = brk || '\n';
                width = width || 75;
                cut = cut || false;

                if (!str) {
                    return str;
                }

                var regex = '.{1,' + width + '}(\\s|$)' + (cut ? '|.{' + width + '}|.+$' :
                    '|\\S+?(\\s|$)');

                return str.match(RegExp(regex, 'g')).join(brk);

            }
            var handle;
            if ((cleanslab.substr(0, 1) == '@') || (cleanslab.substr(0, 3).toUpperCase() ==
                    '#FF')) {
                handle = (cleanslab.substr(0,cleanslab.indexOf(' ')))+' ';


                cleanslab = cleanslab.substr(cleanslab.indexOf(' '));
            } else {
                handle = '';
            }

            if (typeof handle != 'undefined' && handle.length + cleanslab.length >
                140) {
                var apele = $('#info-area')[0];
                var readyslab = wordwrap(cleanslab, 135 - handle.length, '\n');
                var chunks = readyslab.split('\n');
                var chunklen = chunks.length;
                for (var k = 0; k < chunks.length; k++) {
                    if (k === 0) {
                        $('#info-area')[0].innerHTML +='<p>' + handle + chunks[k] + ' …' + (k + 1) + '/' +
                            chunklen + '</p>';
                    } else if (k == (chunklen - 1)) {
                        apele.innerHTML +='<p>' + handle + '…' + chunks[k] + ' ' + (k + 1) +
                            '/' + chunklen + '</p>';
                    } else {
                        apele.innerHTML +='<p>' + handle + '…' + chunks[k] + ' …' + (k + 1) +
                            '/' + chunklen + '</p>';
                    }
                };
            } else {
                if(cleanslab.length > 0 ){
                $('#info-area')[0].innerHTML +='<p>' + handle + ' ' + cleanslab + '</p>';
                }
            }
            toggleSubmit();


        }, 500);
    });

    $('form').on('contextmenu', function(e){
        toggleSubmit();
    });

    $(".advanced").on('click', function(e){
        $('p.reply-to').toggleClass('visuallyhidden');
    });

})(window, function(n,e,k,h,p,m,l,b,d,g,f,c){c=function(a,b){return new c.i(a,b)};c.i=function(a,d){k.push.apply(this,a?a.nodeType||a==n?[a]:""+a===a?/</.test(a)?((b=e.createElement(d||"q")).innerHTML=a,b.children):(d&&c(d)[0]||e).querySelectorAll(a):/f/.test(typeof a)?/c/.test(e.readyState)?a():c(e).on("DOMContentLoaded",a):a:k)};c.i[f="prototype"]=(c.extend=function(a){g=arguments;for(b=1;b<g.length;b++)if(f=g[b])for(d in f)a[d]=f[d];return a})(c.fn=c[f]=k,{on:function(a,d){a=a.split(h);this.map(function(c){(h[b=a[0]+(c.b$=c.b$||++p)]=h[b]||[]).push([d,a[1]]);c["add"+m](a[0],d)});return this},off:function(a,c){a=a.split(h);f="remove"+m;this.map(function(e){if(b=(g=h[a[0]+e.b$])&&g.length)for(;d=g[--b];)c&&c!=d[0]||a[1]&&a[1]!=d[1]||(e[f](a[0],d[0]),g.splice(b,1));else!a[1]&&e[f](a[0],c)});return this},is:function(a){d=(b=this[0])&&(b.matches||b["webkit"+l]||b["moz"+l]||b["ms"+l]);return!!d&&d.call(b,a)}});return c}(window,document,[],/\.(.+)/,0,"EventListener","MatchesSelector"));

