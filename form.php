<div id="info-area">
<?php

use Abraham\TwitterOAuth\TwitterOAuth;


if(isset($_SESSION['response_code']) && $_SESSION['response_code'] != 'tweeted'){
    $total_count = count($_SESSION['tweets']);
    $failed_count = $total_count - $_SESSION['published_count'];
?>
<p class="warning">Tweet  <?php echo $_SESSION['published_count']+1 .' of ' . $total_count ;?> has failed [ <?php echo '<span>'.$_SESSION['error_message'].'</span>' ;?> ].<br />In order to preserve the chronology of your updates we've stopped processing the subsequent updates<br/>
You can try copy &amp; pasting the unpublished tweets manually</p>

<?php

if($total_count > 0){
      $tweets = array_slice($_SESSION['tweets'],$_SESSION['published_count'] );
      foreach ($tweets as $failed) {
        echo '<p>' . $failed . '</p>';
      }
    }
  } else {
    $access_token = $_SESSION['access_token'];
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

    unset($_SESSION['response']);
    if(isset($_SESSION['tweets'])){
      sleep(2.5);
      $status_updates = $connection->get('statuses/user_timeline', array('count' => count($_SESSION['tweets']) ) );
        foreach($status_updates as $check){
        if ($check->source =='<a href="http://www.shearthat.com" rel="nofollow">ShearThat</a>'){
        echo '<p>'.$check->text.'</p>';
        }
      }
    }
  }

// clear unused variables

unset($_SESSION['response']);

?>
    </div>

    <form class="update-form" method="post" action="response.php">
      <div class="options">

        <p class='button advanced'>
          optional
        </p>
      </div>

      <p class="reply-to visuallyhidden">
        <label for="reply-to-id">
          <span>Replying to a tweet?</span>
          <input class="reply-to-id" name="reply_to_id" placeholder="Paste the url of the tweet you're replying to"></label>
      </p>
      <textarea name="the_tweet" id="the_tweet" cols="32" rows="8" placeholder='Enter your long tweet' required='required'></textarea>
      <input type="submit" disabled class="btn btn-large btn-primary" name="update_status" value="tweet">
    </form>