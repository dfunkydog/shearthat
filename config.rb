http_path = "/"
css_dir = "/"
sass_dir ="scss"
images_dir = 'images'
# You can select your preferred output style here (can be overridden via the command line):
#output_style = :expanded or :nested or :compact or :compressed
output_style = :compact

# To enable relative paths to assets via compass helper functions. Uncomment:
#relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false
sass_options = { :sourcemap => true }
enable_sourcemaps = true
