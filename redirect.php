<?php

require "twitteroauth/autoloader.php";

use Abraham\TwitterOAuth\TwitterOAuth;

require "config.php";



$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET)  ;
$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));

$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));

header('Location: ' . $url);

?>