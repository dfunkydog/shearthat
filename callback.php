<?php
session_start();


require 'twitteroauth/autoloader.php';
require "config.php";
use Abraham\TwitterOAuth\TwitterOAuth;


if ( isset($_GET['denied']) ){
	$_SESSION['status'] = 'denied';
	header('Location: index.php' );
}

$request_token = NULL;
$request_token['oauth_token'] = $_GET['oauth_token'];
$request_token['oauth_token_secret'] = $_GET['oauth_verifier'];

if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
    echo "something went wrong";
}

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
$access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier']));
$_SESSION['http_code'] = $connection->getLastHttpCode();
$_SESSION['status'] = 'verified';
$_SESSION['access_token'] = $access_token;

header('Location: index.php' );
?>
